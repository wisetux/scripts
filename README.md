# scripts

Mostly a small collection of my bash scripts.

# graylog-installer

This installation script will perform an automated install of [Graylog](https://www.graylog.org/)
on Ubuntu 18.04. It needs to be run either as root or with sudo privileges.

## Installation steps

```bash
sudo apt install wget -y
wget https://gitlab.com/prasadkumar013/scripts/raw/master/bash-scripts/graylog-installer.sh
chmod +x graylog-installer.sh
sudo ./graylog-installer.sh
```

The script auto detects your IP for you. User would be asked to enter password for 'admin' account.

## Author Info

-   [Blog](https://www.aadya.tech)
